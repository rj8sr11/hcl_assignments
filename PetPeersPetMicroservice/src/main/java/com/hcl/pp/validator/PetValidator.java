package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetValidator implements Serializable {

	private static final long serialVersionUID = 8842211433788795263L;

	@NotNull(message = "pet Name can't be null")
	@NotEmpty(message = "pet Name can't be empty")
	private String name;

	@NotNull(message = "Pet Age Can't be null")
	@Digits(integer = 2, fraction = 0, message = "Age must be in Number ")
	@Min(value = 0, message = "age must be minimum 0 years")
	@Max(value = 99, message = "age must be maximum upto 99 years")
	private Integer age;

	@NotNull(message = "place can't be null")
	@NotEmpty(message = "place can't be empty")
	private String place;
	
	private Long userId;

}
