package com.dao;

import java.sql.Connection;
import java.util.List;

import com.model.Employee;

public interface EmployeeDao {

	public Integer createEmployeeRecord(Connection connection, String employeeName, int employeeAge,
			float employeeSalary);

	public List<Employee> fetchAllEmployeeDetails();

	public Employee findEmployeeByEmployeeId(int employeeId);

	public Boolean deleteEmployeeByEmployeeId(Connection connection, Integer employeeId);
}
