package com.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dao.EmployeeDao;
import com.dbconnection.DBConnection;
import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	private static final Logger logger = LogManager.getLogger(EmployeeDaoImpl.class);

	@Override
	public Integer createEmployeeRecord(Connection connection, String employeeName, int employeeAge,
			float employeeSalary) {
		Integer employeeId = null;
		String insertQuery = "INSERT INTO employee_data.employee(employee_name,employee_age,employee_salary) values(?,?,?)";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, employeeName);
			preparedStatement.setInt(2, employeeAge);
			preparedStatement.setFloat(3, employeeSalary);
			preparedStatement.execute();
			resultSet = preparedStatement.getGeneratedKeys();
			while (resultSet.next()) {
				employeeId = resultSet.getInt(1);
			}
		} catch (Exception e) {
			logger.error("EmployeeDaoImpl.createEmployeeRecord() {}", e.getMessage());
		} finally {
			try {
				resultSet.close();
			} catch (Exception ex) {
				logger.error("Error while closing result set {}", ex);
			}

		}
		return employeeId;
	}

	@Override
	public List<Employee> fetchAllEmployeeDetails() {
		String query = "SELECT * FROM  employee_data.employee";
		Connection connection = DBConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<Employee> employees = new ArrayList<>();
		try {
			preparedStatement = connection.prepareStatement(query);
			resultSet = preparedStatement.executeQuery();
			Employee employee = null;
			while (resultSet.next()) {
				employee = new Employee(resultSet.getString("employee_name"), resultSet.getInt("employee_age"),
						resultSet.getFloat("employee_salary"));
				employees.add(employee);
			}

		} catch (Exception e) {
			logger.error("EmployeeDaoImpl.fetchAllEmployeeDetails() {}", e.getMessage());
		} finally {
			try {
				resultSet.close();
				connection.close();
			} catch (Exception ex) {
				logger.error("Error while closing connection {}", ex);
			}
		}
		return employees;
	}

	@Override
	public Employee findEmployeeByEmployeeId(int employeeId) {
		String query = "SELECT * FROM  employee_data.employee WHERE employee_id = ?";
		Connection connection = DBConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Employee employee = null;
		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, employeeId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				employee = new Employee(resultSet.getString("employee_name"), resultSet.getInt("employee_age"),
						resultSet.getFloat("employee_salary"));
			}
		} catch (Exception e) {
			logger.error("EmployeeDaoImpl.findEmployeeByEmployeeId() {}", e.getMessage());
		} finally {
			try {
				resultSet.close();
				connection.close();
			} catch (Exception ex) {
				logger.error("Error while closing connection {}", ex);
			}
		}
		return employee;
	}

	@Override
	public Boolean deleteEmployeeByEmployeeId(Connection connection, Integer employeeId) {
		String query = "DELETE FROM employee_data.employee WHERE employee_id=?";
		PreparedStatement preparedStatement = null;
		Boolean employeeDeleted = null;
		try {
			if (findEmployeeByEmployeeId(employeeId) != null) {
				preparedStatement = connection.prepareStatement(query);
				preparedStatement.setInt(1, employeeId);
				preparedStatement.executeUpdate();
				employeeDeleted = true;
			} else {
				employeeDeleted = false;
			}

		} catch (Exception e) {
			logger.error("EmployeeDaoImpl.deleteEmployeeByEmployeeId() {}", e.getMessage());
		}
		return employeeDeleted;
	}

}
