package com.service.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dao.EmployeeDao;
import com.dao.impl.EmployeeDaoImpl;
import com.dbconnection.DBConnection;
import com.model.Employee;
import com.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger logger = LogManager.getLogger(EmployeeServiceImpl.class);

	@Override
	public Integer createEmployeeRecord(String employeeName, Integer employeeAge, Float employeeSalary)
			throws Exception {
		Connection connection = null;
		Integer employeeId = null;
		while (true) {
			if (!(employeeName.matches("^(?![ .]+$)[a-zA-Z .]*$"))) {
				logger.info("Name must contain only alphabets");
				break;
			} else if (employeeAge < 18) {
				logger.info("Employee Age must be greater than 18");
				break;
			} else if (employeeSalary < 100) {
				logger.info("Employee Salary must be greater than 100");
				break;
			} else {
				try {
					connection = DBConnection.getConnection();
					EmployeeDao employeeDao = new EmployeeDaoImpl();
					connection.setAutoCommit(false);
					employeeId = employeeDao.createEmployeeRecord(connection, employeeName, employeeAge,
							employeeSalary);
					connection.commit();

				} catch (Exception ex) {
					try {
						connection.rollback();
					} catch (Exception e) {
						logger.error("Error while inserting record of an employee {}", e);
					}
				} finally {
					try {
						connection.close();
						break;
					} catch (Exception ex) {
						logger.error("Error while closing connection {}", ex);
					}
				}
			}
		}
		return employeeId;
	}

	@Override
	public List<Employee> fetchAllEmployeeDetails() {
		List<Employee> employees = new ArrayList<>();
		try {
			EmployeeDao employeeDao = new EmployeeDaoImpl();
			employees = employeeDao.fetchAllEmployeeDetails();
		} catch (Exception ex) {
			logger.error("Error while fetching record of the employees {}", ex);
		}

		return employees;
	}

	@Override
	public Employee findEmployeeByEmployeeId(int employeeId) {

		Employee employees = null;

		try {
			if (String.valueOf(employeeId).matches("^[0-9]*$")) {
				EmployeeDao employeeDao = new EmployeeDaoImpl();
				employees = employeeDao.findEmployeeByEmployeeId(employeeId);
			} else {
				logger.info("Invalid Employee ID");
			}
		} catch (Exception ex) {
			logger.error("Error while fetching record of the employees by employee Id {}", ex);
		}
		return employees;
	}

	@Override
	public Boolean deleteEmployeeByEmployeeId(Integer employeeId) {
		Boolean employeeDeleted = null;
		Connection connection = null;
		try {
			if (String.valueOf(employeeId).matches("^[0-9]*$")) {
				connection = DBConnection.getConnection();
				EmployeeDao employeeDao = new EmployeeDaoImpl();
				connection.setAutoCommit(false);
				employeeDeleted = employeeDao.deleteEmployeeByEmployeeId(connection, employeeId);
				connection.commit();
			} else {
				logger.info("Invalid Employee ID");
			}
		} catch (Exception ex) {
			try {
				connection.rollback();
			} catch (Exception e) {
				logger.error("Error while deleting record of an employee {}", e);
			}
		} finally {
			try {
				connection.close();
			} catch (Exception ex) {
				logger.error("Error while closing connection {}", ex);
			}
		}
		return employeeDeleted;
	}

}
