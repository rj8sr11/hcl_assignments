package com.service;

import java.util.List;

import com.model.Employee;

public interface EmployeeService {
	public Integer createEmployeeRecord(String employeeName, Integer employeeAge, Float employeeSalary)
			throws Exception;

	public List<Employee> fetchAllEmployeeDetails();

	public Employee findEmployeeByEmployeeId(int employeeId);

	public Boolean deleteEmployeeByEmployeeId(Integer employeeId);
}
