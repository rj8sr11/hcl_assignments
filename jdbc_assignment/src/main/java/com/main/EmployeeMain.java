package com.main;

import java.util.List;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.model.Employee;
import com.service.EmployeeService;
import com.service.impl.EmployeeServiceImpl;

public class EmployeeMain {

	private static final Logger logger = LogManager.getLogger(EmployeeMain.class);

	public static void main(String[] args) {
		logger.info("STARTED");
		Scanner scanner = new Scanner(System.in);
		char ch = 0;
		do {
			System.out.println("MENU : ");
			System.out.println("1) Create an employee ");
			System.out.println("2) Fetch all employees detail ");
			System.out.println("3) Fetch Employee by EmployeeID ");
			System.out.println("4) Delete Employee by EmployeeID ");

			int choice = scanner.nextInt();
			scanner.nextLine();
			if (choice == 1) {
				System.out.println("Enter employee name (ONLY ALPHABETS)");
				String employeeName = scanner.nextLine();
				System.out.println("Enter employee age (SHOULD BE GREATER THAN 18)");
				int age = scanner.nextInt();
				System.out.println("Enter employee salary (SHOULD BE GREATER THAN 100)");
				float salary = scanner.nextFloat();

				EmployeeService employeeService = new EmployeeServiceImpl();
				try {
					Integer employeeId = employeeService.createEmployeeRecord(employeeName, age, salary);
					if (employeeId != null) {
						logger.info("Employee Record successfully inserted with employeeId: " + employeeId);
					}
					// else {
					// logger.info("Employee not inserted");
					// }
				} catch (Exception e) {
					logger.error("Error while inserting record of an employee {}", e);
				} finally {
					employeeService = null;
				}
			} else if (choice == 2) {
				try {
					EmployeeService employeeService = new EmployeeServiceImpl();
					List<Employee> employees = employeeService.fetchAllEmployeeDetails();
					if (employees != null && employees.size() > 0) {
						logger.info("----------- SUCCESSFULLY FETCHED EMPLOYEES DETAILS ------------");
						employees.stream()
								.forEach(employeeObj -> logger.info("Employee Name : " + employeeObj.getEmployeeName()
										+ " Employee Age: " + employeeObj.getEmployeeAge() + " Employee Salary: "
										+ employeeObj.getEmployeeSalary()));
					} else {
						logger.info("No Employee Record exists");
					}
				} catch (Exception e) {
					logger.error("Exception while fetching All employee details ", e);
				}
			} else if (choice == 3) {
				try {
					EmployeeService employeeService = new EmployeeServiceImpl();
					System.out.println("Enter employee ID ");
					while (scanner.hasNext()) {
						if (scanner.hasNextInt()) {
							final Integer employeeId = scanner.nextInt();
							if (String.valueOf(employeeId).matches("^[0-9]*$")) {
								Employee employeeDetail = employeeService.findEmployeeByEmployeeId(employeeId);
								if (employeeDetail != null) {
									logger.info("--- EMPLOYEE EXISTS ----");
									logger.info("Employee Name : " + employeeDetail.getEmployeeName()
											+ " Employee Age: " + employeeDetail.getEmployeeAge() + " Employee Salary: "
											+ employeeDetail.getEmployeeSalary());
									break;
								} else {
									logger.info("Employee Doesn't Exists");
									break;
								}
							}
						} else {
							logger.info("Employee ID must be a number");
							scanner.nextLine();
							break;
						}
					}
				} catch (Exception e) {
					logger.error("Exception while fetching employee details by employee Id ", e);
				}

			}
			if (choice == 4) {
				try {
					EmployeeService employeeService = new EmployeeServiceImpl();
					System.out.println("Enter employee ID ");
					while (scanner.hasNext()) {
						if (scanner.hasNextInt()) {
							final Integer employeeId = scanner.nextInt();
							if (String.valueOf(employeeId).matches("^[0-9]*$")) {
								Boolean employeeDeleted = employeeService.deleteEmployeeByEmployeeId(employeeId);
								if (employeeDeleted == true) {
									logger.info("--- EMPLOYEE DELETED SUCCESSFULLY ----");
									break;
								} else {
									logger.info("Employee Doesn't Exists");
									break;
								}
							}
						} else {
							logger.info("Employee ID must be a number");
							scanner.nextLine();
							break;
						}
					}
				} catch (Exception e) {
					logger.error("Exception while deleting employee details by employee Id ", e);
				}
			}

			System.out.println(" DO YOU WANT TO CONTINUE (Y / N) : ");

			ch = scanner.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		logger.info("Main ended");
		scanner.close();
	}

}
