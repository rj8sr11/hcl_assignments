package com.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.exception.CustomException;
import com.model.Department;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.impl.EmployeeServiceImpl;

/**
 * @author rajat sharma
 *
 */
public class EmployeeMain {

	private static final Logger logger = LogManager.getLogger(EmployeeMain.class);

	public static void main(String[] args) {
		logger.info("Main STARTED");
		Scanner scanner = new Scanner(System.in);
		char ch = 0;
		do {
			System.out.println("MENU : ");
			System.out.println("1) Create an employee ");
			System.out.println("2) Fetch all employees detail ");
			System.out.println("3) Fetch Employee by EmployeeID ");
			System.out.println("4) Delete Employee by EmployeeID ");

			int choice = scanner.nextInt();
			scanner.nextLine();
			if (choice == 1) {
				try {
					System.out.println("Enter Employee FirstName:");
					String firstName = scanner.nextLine();
					System.out.println("Enter Department Name:");
					String departmentName = scanner.nextLine();

					EmployeeService employeeService = new EmployeeServiceImpl();
					Department department = new Department();
					department.setDepartmentName(departmentName);
					Employee employee = new Employee();
					employee.setFirstname(firstName);
					employee.setDepartment(department);
					Optional<Employee> employeeDetails = employeeService.saveEmployee(employee);
					if (employeeDetails.isPresent()) {
						Employee employeeDetail = employeeDetails.get();
						logger.info("Employee Created With Details\n" + "EmployeeName: " + employeeDetail.getFirstname()
								+ "Employee Department:" + employeeDetail.getDepartment().getDepartmentName());
					}
					employeeService = null;
				} catch (CustomException e) {
					logger.error("Exception in Main while creating employee", e);
				}
			}
			if (choice == 2) {
				List<Employee> employeeDetails = new ArrayList<>();
				try {
					EmployeeService employeeService = new EmployeeServiceImpl();
					employeeDetails = employeeService.findAllEmployee();
					if (employeeDetails != null && employeeDetails.size() > 0) {
						employeeDetails.stream()
								.forEach(employee -> logger.info("EmployeeName :" + employee.getFirstname()
										+ "\nEmployee Department:" + employee.getDepartment().getDepartmentName()));
					} else {
						logger.info("NO RECORD");
					}
					employeeService = null;
				} catch (CustomException e) {
					logger.error("Exception while fetching all employeeDetails in Main method" + e);
				}
			}
			if (choice == 3) {
				try {
					System.out.println("Enter employee ID ");
					final Integer employeeId = scanner.nextInt();
					EmployeeService employeeService = new EmployeeServiceImpl();
					Optional<Employee> employeeDetails = employeeService.findEmployeeByEmployeeId(employeeId);
					if (employeeDetails.isPresent()) {
						logger.info("EmployeeExists with Data\n EmployeeName :" + employeeDetails.get().getFirstname()
								+ "\nEmployee Department:" + employeeDetails.get().getDepartment().getDepartmentName());
					} else {
						logger.info("NO Employee Exists with the given employee Id");
					}
					employeeService = null;
				} catch (CustomException e) {
					logger.error("Exception while fetching employeeDetails by employeeId in Main method" + e);
				}
			}
			if (choice == 4) {
				try {
					EmployeeService employeeService = new EmployeeServiceImpl();
					System.out.println("Enter employee ID ");
					while (scanner.hasNext()) {
						if (scanner.hasNextInt()) {
							final Integer employeeId = scanner.nextInt();
							if (String.valueOf(employeeId).matches("^[0-9]*$")) {
								Integer employeeDeleted = employeeService.deletEmployeeByEmployeeId(employeeId);
								if (employeeDeleted == employeeId) {
									logger.info("--- EMPLOYEE DELETED SUCCESSFULLY WITH EMPLOYEE ID: ----"
											+ employeeDeleted);
									break;
								} else {
									logger.info("Employee Doesn't Exists");
									break;
								}
							}
						} else {
							logger.info("Employee ID must be a number");
							scanner.nextLine();
							break;
						}
					}
				} catch (Exception e) {
					logger.error("Exception while deleting employee details by employee Id ", e);
				}
			}

			System.out.println(" DO YOU WANT TO CONTINUE (Y / N) : ");
			ch = scanner.next().charAt(0);
		} while (ch == 'Y' || ch == 'y');
		scanner.close();
		logger.info("Main ended");
	}

}
