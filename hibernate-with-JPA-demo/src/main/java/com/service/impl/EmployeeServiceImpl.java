package com.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dao.EmployeeDao;
import com.dao.impl.EmployeeDaoImpl;
import com.exception.CustomException;
import com.model.Employee;
import com.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger logger = LogManager.getLogger(EmployeeServiceImpl.class);

	@Override
	public Optional<Employee> saveEmployee(Employee employee) throws CustomException {
		logger.info("inside saveEmployee() serviceImpl");
		Optional<Employee> employeeDetails = null;
		if (employee.getFirstname() != null && employee.getDepartment().getDepartmentName() != null) {
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence_xml");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EmployeeDao employeeDao = new EmployeeDaoImpl(entityManager);
			employeeDetails = employeeDao.saveEmployee(employee);
		} else {
			throw new CustomException("INVALID INPUTS");
		}
		return employeeDetails;
	}

	@Override
	public Optional<Employee> findEmployeeByEmployeeId(Integer employeeId) throws CustomException {
		logger.info("inside findEmployeeByEmployeeId() serviceImpl");
		Optional<Employee> employeeDetails = null;
		if (String.valueOf(employeeId).length() > 0) {
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence_xml");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EmployeeDao employeeDao = new EmployeeDaoImpl(entityManager);
			employeeDetails = employeeDao.findEmployeeByEmployeeId(employeeId);
		} else {
			throw new CustomException("INVALID INPUTS");
		}
		return employeeDetails;
	}

	@Override
	public List<Employee> findAllEmployee() throws CustomException {
		logger.info("inside findAllEmployee() serviceImpl");

		List<Employee> employeeDetails = new ArrayList<>();
		try {
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence_xml");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EmployeeDao employeeDao = new EmployeeDaoImpl(entityManager);
			employeeDetails = employeeDao.findAllEmployee();
		} catch (CustomException e) {
			throw new CustomException("No Record Found");
		}
		return employeeDetails;
	}

	@Override
	public Integer deletEmployeeByEmployeeId(Integer employeeId) throws CustomException {
		logger.info("inside deletEmployeeByEmployeeId() serviceImpl");
		Integer employeeDeleted = null;
		try {
			EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("persistence_xml");
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			EmployeeDao employeeDao = new EmployeeDaoImpl(entityManager);
			employeeDeleted = employeeDao.deletEmployeeByEmployeeId(employeeId);
		} catch (CustomException e) {
			throw new CustomException("Exception while deleting record of an employee");
		}
		return employeeDeleted;
	}

}
