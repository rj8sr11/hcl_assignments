package com.service;

import java.util.List;
import java.util.Optional;

import com.exception.CustomException;
import com.model.Employee;

public interface EmployeeService {

	public Optional<Employee> saveEmployee(Employee employee) throws CustomException;;

	public Optional<Employee> findEmployeeByEmployeeId(Integer employeeId) throws CustomException;

	public List<Employee> findAllEmployee() throws CustomException;

//	public Optional<Employee> update(Employee employee);

	public Integer deletEmployeeByEmployeeId(Integer employeeId) throws CustomException;
}
