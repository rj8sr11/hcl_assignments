package com.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dao.EmployeeDao;
import com.exception.CustomException;
import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	private static final Logger logger = LogManager.getLogger(EmployeeDaoImpl.class);

	private EntityManager entityManager;

	public EmployeeDaoImpl(EntityManager entityManager) {
		super();
		this.entityManager = entityManager;
	}

	@Override
	public Optional<Employee> saveEmployee(Employee employee) throws CustomException {
		logger.info("Inside saveEmployee() Dao Impl");
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(employee.getDepartment());
			entityManager.persist(employee);
			entityManager.getTransaction().commit();
			return Optional.of(employee);
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			throw new CustomException("Exception While Saving Employee Details");
		}
	}

	@Override
	public Optional<Employee> findEmployeeByEmployeeId(Integer employeeId) throws CustomException {
		logger.info("Inside findEmployeeByEmployeeId() Dao Impl");
		try {
			Employee employee = entityManager.find(Employee.class, employeeId);
			return employee != null ? Optional.of(employee) : Optional.empty();
		} catch (Exception e) {
			throw new CustomException("Exception While findEmployeeByEmployeeId Dao Layer");

		}
	}

	@Override
	public List<Employee> findAllEmployee() throws CustomException {
		logger.info("Inside findAllEmployee() Dao Impl");

		List<Employee> employees = new ArrayList<>();
		try {
			employees = entityManager.createQuery("from Employee", Employee.class).getResultList();
		} catch (Exception e) {
			throw new CustomException("Exception While findEmployeeByEmployeeId Dao Layer");
		}
		return employees;
	}

	@Override
	public Integer deletEmployeeByEmployeeId(Integer employeeId) throws CustomException {
		logger.info("Inside deletEmployeeByEmployeeId() Dao Impl");
		try {
			Employee employee = entityManager.find(Employee.class, employeeId);
			entityManager.getTransaction().begin();
			entityManager.remove(employee);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			throw new CustomException("Exception While deletEmployeeByEmployeeId Dao Layer");
		}
		return employeeId;
	}

}
