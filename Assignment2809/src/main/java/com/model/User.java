package com.model;

public class User {
	private int userId;
	private String userFirstName;
	private String userPassword;

	public User() {
		super();
	}

	public User(int userId, String userFirstName, String userPassword) {
		super();
		this.userId = userId;
		this.userFirstName = userFirstName;
		this.userPassword = userPassword;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

}
