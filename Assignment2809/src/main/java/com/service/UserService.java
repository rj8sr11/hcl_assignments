package com.service;

import com.model.User;

public interface UserService {
	public User authenticatingUser(int userId, String password);
}
