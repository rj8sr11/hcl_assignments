package com.service.impl;

import com.dao.UserDao;
import com.dao.impl.UserDaoImpl;
import com.model.User;
import com.service.UserService;

public class UserServiceImpl implements UserService {

	public User authenticatingUser(int userId, String password) {
		User user = null;
		try {
			if (String.valueOf(userId).length() >= 6 && password.length() >= 6) {
				UserDao userDao = new UserDaoImpl();
				user = userDao.authenticatingUser(userId, password);
			}
		} catch (Exception e) {
			System.err.println("WRONG CREDENTIALS");
		}
		return user;
	}
}
