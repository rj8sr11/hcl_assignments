package com.dao;

import com.model.User;

public interface UserDao {
	public User authenticatingUser(int userId, String password);
}
