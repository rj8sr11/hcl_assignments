package com.main;

import java.util.Scanner;

import com.model.User;
import com.service.UserService;
import com.service.impl.UserServiceImpl;

public class UserMain {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter your userId");
		int userId = scanner.nextInt();

		System.out.println("Enter your pasword");
		String password = scanner.next();

		UserService userService = new UserServiceImpl();

		User userDetails = null;
		try {
			userDetails = userService.authenticatingUser(userId, password);
			System.out.println("WELCOME : " + userDetails.getUserFirstName());
		} catch (Exception e) {
			System.err.println("WRONG CREDENTIALS");
		} finally {
			userService = null;
			scanner.close();
		}
	}
}
