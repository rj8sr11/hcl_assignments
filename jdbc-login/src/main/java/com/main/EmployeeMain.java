package com.main;

import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.exception.CustomException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.impl.EmployeeServiceImpl;

public class EmployeeMain {

	private static final Logger logger = LogManager.getLogger(EmployeeMain.class);

	public static void main(String[] args) {
		logger.info("Main STARTED");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee UserName");
		String userName = scanner.next();
		System.out.println("Enter Password");
		String password = scanner.next();

		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee employeeDetails = null;
		try {
			employeeDetails = employeeService.authenticatingEmployeeDetails(userName, password);
			if (employeeDetails != null) {
				logger.info("WELCOME \n" + "EmployeeName :" + employeeDetails.getEmployeeName() + "\nEmployeeAge:"
						+ employeeDetails.getEmployeeAge());
			} else {
				logger.info("INVALID CREDENTIALS");
			}
		} catch (CustomException e) {
			logger.error("Exception while authentication employeeDetails in Main method" + e);
		} finally {
			scanner.close();
		}
		logger.info("Main ended");
	}

}
