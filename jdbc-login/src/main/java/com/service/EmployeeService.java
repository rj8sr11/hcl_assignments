package com.service;

import com.exception.CustomException;
import com.model.Employee;

public interface EmployeeService {

	public Employee authenticatingEmployeeDetails(String userName, String password) throws CustomException;
}
