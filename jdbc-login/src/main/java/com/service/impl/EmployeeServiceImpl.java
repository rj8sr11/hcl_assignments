package com.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dao.EmployeeDao;
import com.dao.impl.EmployeeDaoImpl;
import com.exception.CustomException;
import com.model.Employee;
import com.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger logger = LogManager.getLogger(EmployeeServiceImpl.class);

	@Override
	public Employee authenticatingEmployeeDetails(String userName, String password) throws CustomException {
		logger.info("Inside Service Layer authenticatingEmployeeDetails()");
		Employee employee = null;

		if (userName.length() >= 5 && password.length() >= 6) {
			EmployeeDao employeeDao = new EmployeeDaoImpl();
			employee = employeeDao.authenticatingEmployeeDetails(userName, password);
		} else {
			throw new CustomException("INVALID INPUTS");
		}
		return employee;
	}

}
