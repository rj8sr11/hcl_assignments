package com.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.constants.Constants;
import com.exception.CustomException;

public class DBConnection {

	private static final Logger logger = LogManager.getLogger(DBConnection.class);

	public static Connection getConnection() throws CustomException {
		logger.info("getConnection() in DBConnection");
		Connection connection = null;
		try {
			Class.forName(Constants.DRIVERNAME);
			connection = DriverManager.getConnection(Constants.URL, Constants.USERNAME, Constants.PASSWORD);
		} catch (Exception e) {
			logger.error("Exception in getConnection (DBConnection)", e);
		}
		return connection;
	}
}
