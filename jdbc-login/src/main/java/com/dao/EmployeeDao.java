package com.dao;

import com.exception.CustomException;
import com.model.Employee;

public interface EmployeeDao {

	public Employee authenticatingEmployeeDetails(String userName, String password) throws CustomException;

}
