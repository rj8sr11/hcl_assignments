package com.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dao.EmployeeDao;
import com.dbconnection.DBConnection;
import com.exception.CustomException;
import com.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	private static final Logger logger = LogManager.getLogger(EmployeeDaoImpl.class);

	@Override
	public Employee authenticatingEmployeeDetails(String userName, String password) throws CustomException {
		logger.info("Inside Dao Layer authenticatingEmployeeDetails()");

		Connection connection = DBConnection.getConnection();
		Employee employee = null;
		String query = "SELECT emp_name,emp_age FROM  employee_data.employee_login_details where emp_username = ? AND emp_password = ?";
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, userName);
			preparedStatement.setString(2, password);

			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				employee = new Employee(resultSet.getString("emp_name"), resultSet.getInt("emp_age"));
			}

		} catch (Exception e) {
			throw new CustomException("EmployeeDaoImpl.authenticatingEmployeeDetails()");
		} finally {
			try {
				resultSet.close();
				connection.close();
			} catch (Exception ex) {
				logger.error("Error while closing connection {}", ex);
			}
		}
		return employee;
	}

}
