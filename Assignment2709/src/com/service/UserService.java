package com.service;

import com.model.User;

public interface UserService {

	public User authenticatingUserDetail(String userName, String password);
}
