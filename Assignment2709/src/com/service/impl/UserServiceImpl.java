package com.service.impl;

import com.dao.UserDao;
import com.model.User;
import com.service.UserService;

public class UserServiceImpl implements UserService {

	@Override
	public User authenticatingUserDetail(String userName, String password) {
		User[] userRecords = UserDao.fetchUserRecords();
		for (int i = 0; i < userRecords.length; i++) {
			if (userRecords[i].getUserName().equals(userName.toUpperCase())
					&& userRecords[i].getUserPassword().equals(password)) {
				return userRecords[i];
			}
		}
		return null;
	}

}
