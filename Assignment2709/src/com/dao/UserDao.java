package com.dao;

import com.model.User;

public class UserDao {

	public static User[] fetchUserRecords() {
		User employee1 = new User(10, "MA LIN", "MA_LIN", "1234");
		User employee2 = new User(20, "LEBRON JAMES", "L_JAMES", "5678");
		User employee3 = new User(30, "KYRE IRVING", "K_NBA", "1234KV");
		User employee4 = new User(40, "RAJAT", "RJ8SR", "VAT69");

		User[] userRecords = new User[4];
		userRecords[0] = employee1;
		userRecords[1] = employee2;
		userRecords[2] = employee3;
		userRecords[3] = employee4;

		return userRecords;
	}

}
