package com.main;

import java.util.Scanner;

import com.model.User;
import com.service.UserService;
import com.service.impl.UserServiceImpl;

public class UserMain {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter your username: ");
		String userName = scanner.next();

		System.out.println("Enter your password: ");
		String userPassword = scanner.next();

		UserService userService = new UserServiceImpl();
		User userDetail = userService.authenticatingUserDetail(userName, userPassword);
		if (userDetail != null) {
			System.out.println("Welcome : " + userDetail.getUserFirstName());
		} else {
			System.out.println("INVALID CREDENTIALS");
		}

		userService = null;
		scanner.close();

	}
}
