package com.employee.management.responses;

public class ResponseBuilder {

	private ResponseBuilder() {
		super();
	}

	public static BaseApiResponse created(Object responseData) {

		BaseApiResponse baseApiResponse = new BaseApiResponse();
		baseApiResponse.setResponseStatus(new ResponseStatus(201));
		baseApiResponse.setResponseData(responseData);

		return baseApiResponse;
	}

	public static BaseApiResponse resourceNotFound(Object responseData) {
		BaseApiResponse baseApiResponse = new BaseApiResponse();
		baseApiResponse.setResponseStatus(new ResponseStatus(404));
		baseApiResponse.setResponseData(responseData);
		return baseApiResponse;
	}

	public static BaseApiResponse unAuthorizedAccess(Object responseData) {
		BaseApiResponse baseApiResponse = new BaseApiResponse();
		baseApiResponse.setResponseStatus(new ResponseStatus(401));
		baseApiResponse.setResponseData(responseData);
		return baseApiResponse;
	}

	public static BaseApiResponse conflict(Object responseData) {
		BaseApiResponse baseApiResponse = new BaseApiResponse();
		baseApiResponse.setResponseStatus(new ResponseStatus(409));
		baseApiResponse.setResponseData(responseData);
		return baseApiResponse;
	}

	public static BaseApiResponse getSuccessResponse(Object responseData) {
		BaseApiResponse baseApiResponse = new BaseApiResponse();
		baseApiResponse.setResponseStatus(new ResponseStatus(200));
		baseApiResponse.setResponseData(responseData);
		return baseApiResponse;
	}
	
	public static BaseApiResponse badRequest(Object responseData) {
		BaseApiResponse baseApiResponse = new BaseApiResponse();
		baseApiResponse.setResponseStatus(new ResponseStatus(400));
		baseApiResponse.setResponseData(responseData);
		return baseApiResponse;
	}
}