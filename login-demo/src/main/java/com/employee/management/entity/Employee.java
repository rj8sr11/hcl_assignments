package com.employee.management.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "tbl_employee_auth")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id", columnDefinition = "INT UNSIGNED")
	private Integer employeeId;

	@Column(name = "emp_name", length = 50)
	@NotNull
	private String employeeName;

	@Column(name = "emp_age", columnDefinition = "INT UNSIGNED")
	@NotNull
	private Integer employeeAge;

	@Column(name = "emp_username", length = 100, unique = true)
	@NotNull
	private String userName;

	@Column(name = "emp_password", length = 50)
	@NotNull
	private String password;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "emp_created", nullable = false, updatable = false, columnDefinition = "DATETIME")
	private Calendar employeeCreated;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	@Column(name = "emp_modified", columnDefinition = "DATETIME")
	private Calendar employeeModified;

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(Integer employeeAge) {
		this.employeeAge = employeeAge;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Calendar getEmployeeCreated() {
		return employeeCreated;
	}

	public void setEmployeeCreated(Calendar employeeCreated) {
		this.employeeCreated = employeeCreated;
	}

	public Calendar getEmployeeModified() {
		return employeeModified;
	}

	public void setEmployeeModified(Calendar employeeModified) {
		this.employeeModified = employeeModified;
	}

}
