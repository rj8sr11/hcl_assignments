package com.employee.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.management.customexception.CustomException;
import com.employee.management.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	Employee findByUserNameAndPassword(String userName, String password) throws CustomException;

	Employee findByUserName(String userName);

}
