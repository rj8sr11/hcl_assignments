package com.employee.management.customexception;

public class CustomException extends Exception {

	private static final long serialVersionUID = -7104894509297913527L;

	private String customMessage;

	public CustomException(String customMessage) {
		super();
		this.customMessage = customMessage;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	@Override
	public String getMessage() {
		return customMessage;
	}

}