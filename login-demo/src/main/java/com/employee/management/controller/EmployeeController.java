package com.employee.management.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.management.customexception.CustomException;
import com.employee.management.request.dto.EmployeeAuthRequestDto;
import com.employee.management.request.dto.EmployeeRequestDto;
import com.employee.management.request.dto.EmployeeUpdateRequestDto;
import com.employee.management.response.dto.EmployeeAuthResponseDto;
import com.employee.management.response.dto.EmployeeDetailsResponseDto;
import com.employee.management.responses.BaseApiResponse;
import com.employee.management.responses.ResponseBuilder;
import com.employee.management.service.EmployeeService;

/**
 * @author rajat sharma
 *
 */
@RestController
@RequestMapping("/v1")
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	EmployeeService employeeService;

	private static ResponseEntity<BaseApiResponse> responseEntity = null;
	private static BaseApiResponse baseApiResponse = null;

	@PostMapping("/authenticate/employee")
	public @ResponseBody ResponseEntity<BaseApiResponse> authenticateEmployee(
			@RequestBody @Valid EmployeeAuthRequestDto employeeRequestDto, BindingResult result)
			throws CustomException {
		if (result.hasErrors()) {
			String errorMessage = "";
			List<ObjectError> errors = result.getAllErrors();
			for (ObjectError e : errors) {
				errorMessage += e.getDefaultMessage();
				baseApiResponse = ResponseBuilder.badRequest(errorMessage);
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.BAD_REQUEST);
			}
		} else {
			EmployeeAuthResponseDto employeeResponseDto = null;
			try {
				employeeResponseDto = employeeService.authenticatingEmployeeDetails(employeeRequestDto);
				if (employeeResponseDto != null) {
					baseApiResponse = ResponseBuilder.getSuccessResponse(employeeResponseDto);
					responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.OK);
				}
			} catch (CustomException e) {
				logger.error("Exception while authenticate Employee: {}", e.getCustomMessage());
				baseApiResponse = ResponseBuilder.unAuthorizedAccess(e.getCustomMessage());
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.UNAUTHORIZED);
			}

		}
		return responseEntity;
	}

	@PostMapping("/create/employee")
	public @ResponseBody ResponseEntity<BaseApiResponse> createEmployee(
			@RequestBody @Valid EmployeeRequestDto employeeCreateRequestDto, BindingResult result)
			throws CustomException {
		if (result.hasErrors()) {
			String errorMessage = "";
			List<ObjectError> errors = result.getAllErrors();
			for (ObjectError e : errors) {
				errorMessage += e.getDefaultMessage();
				baseApiResponse = ResponseBuilder.badRequest(errorMessage);
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.BAD_REQUEST);
			}
		} else {
			EmployeeDetailsResponseDto employeeResponseDto = null;
			try {
				employeeResponseDto = employeeService.createEmployee(employeeCreateRequestDto);
				if (employeeResponseDto != null) {
					baseApiResponse = ResponseBuilder.created(employeeResponseDto);
					responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.CREATED);
				}
			} catch (CustomException e) {
				logger.error("Exception while creating Employee: {}", e.getCustomMessage());
				baseApiResponse = ResponseBuilder.conflict(e.getCustomMessage());
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.CONFLICT);
			}
		}
		return responseEntity;
	}

	@GetMapping("/fetch/all/employees")
	public @ResponseBody ResponseEntity<BaseApiResponse> fetchAllEmployeeDetails() throws CustomException {
		List<EmployeeDetailsResponseDto> employeesDetailsResponseDto = new ArrayList<>();
		try {
			employeesDetailsResponseDto = employeeService.fetchAllEmployees();
			if (employeesDetailsResponseDto != null && employeesDetailsResponseDto.size() > 0) {
				baseApiResponse = ResponseBuilder.getSuccessResponse(employeesDetailsResponseDto);
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.OK);
			}
		} catch (CustomException e) {
			logger.error("Exception while fetching Employees Details: {}", e.getCustomMessage());
			baseApiResponse = ResponseBuilder.resourceNotFound(e.getCustomMessage());
			responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}

	@GetMapping("/fetch/employee/{employeeId}")
	public @ResponseBody ResponseEntity<BaseApiResponse> fetchEmployeeByEmployeeId(
			@PathVariable("employeeId") Integer employeeId) throws CustomException {
		EmployeeDetailsResponseDto employeesDetailsResponseDto = null;
		try {
			employeesDetailsResponseDto = employeeService.fetchEmployeeByEmployeeId(employeeId);
			if (employeesDetailsResponseDto != null) {
				baseApiResponse = ResponseBuilder.getSuccessResponse(employeesDetailsResponseDto);
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.OK);
			}
		} catch (CustomException e) {
			logger.error("Exception while fetching Employees Details by employeeId: {}", e.getCustomMessage());
			baseApiResponse = ResponseBuilder.resourceNotFound(e.getCustomMessage());
			responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}

	@PutMapping("/update/employee/{employeeId}")
	public @ResponseBody ResponseEntity<BaseApiResponse> updateEmployeeName(
			@RequestBody @Valid EmployeeUpdateRequestDto employeeUpdateRequestDto, BindingResult result,
			@PathVariable("employeeId") Integer employeeId) throws CustomException {
		if (result.hasErrors()) {
			String errorMessage = "";
			List<ObjectError> errors = result.getAllErrors();
			for (ObjectError e : errors) {
				errorMessage += e.getDefaultMessage();
				baseApiResponse = ResponseBuilder.badRequest(errorMessage);
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.BAD_REQUEST);
			}
		} else {
			EmployeeDetailsResponseDto employeeResponseDto = null;
			try {
				employeeResponseDto = employeeService.updateEmployeeName(employeeUpdateRequestDto, employeeId);
				if (employeeResponseDto != null) {
					baseApiResponse = ResponseBuilder.created(employeeResponseDto);
					responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.CREATED);
				}
			} catch (CustomException e) {
				logger.error("Exception while updating Employee: {}", e.getCustomMessage());
				baseApiResponse = ResponseBuilder.conflict(e.getCustomMessage());
				responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.CONFLICT);
			}
		}
		return responseEntity;
	}

	@DeleteMapping("/delete/employee/{employeeId}")
	public @ResponseBody ResponseEntity<BaseApiResponse> deleteEmployeeByEmployeeId(
			@PathVariable("employeeId") Integer employeeId) throws CustomException {
		try {
			employeeService.deleteEmployeeByEmployeeId(employeeId);
			baseApiResponse = ResponseBuilder.getSuccessResponse("Deleted Successfully");
			responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.OK);
		} catch (CustomException e) {
			logger.error("Exception while deleting Employees Details by employeeId: {}", e.getCustomMessage());
			baseApiResponse = ResponseBuilder.resourceNotFound(e.getCustomMessage());
			responseEntity = new ResponseEntity<>(baseApiResponse, HttpStatus.NOT_FOUND);
		}
		return responseEntity;
	}

}
