package com.employee.management.response.dto;

import java.io.Serializable;

public class EmployeeAuthResponseDto implements Serializable {

	private static final long serialVersionUID = -7422052889454052585L;

	private String employeeName;
	private Integer employeeAge;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(Integer employeeAge) {
		this.employeeAge = employeeAge;
	}

}
