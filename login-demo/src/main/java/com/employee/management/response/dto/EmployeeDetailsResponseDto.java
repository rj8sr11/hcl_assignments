package com.employee.management.response.dto;

import java.io.Serializable;
import java.util.Calendar;

public class EmployeeDetailsResponseDto implements Serializable {

	private static final long serialVersionUID = 5301542864046203006L;

	private Integer employeeId;

	private String employeeName;

	private Integer employeeAge;

	private Calendar employeeCreated;

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(Integer employeeAge) {
		this.employeeAge = employeeAge;
	}

	public Calendar getEmployeeCreated() {
		return employeeCreated;
	}

	public void setEmployeeCreated(Calendar employeeCreated) {
		this.employeeCreated = employeeCreated;
	}

}
