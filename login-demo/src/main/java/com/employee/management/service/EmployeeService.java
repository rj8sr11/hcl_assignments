package com.employee.management.service;

import java.util.List;

import javax.validation.Valid;

import com.employee.management.customexception.CustomException;
import com.employee.management.request.dto.EmployeeAuthRequestDto;
import com.employee.management.request.dto.EmployeeRequestDto;
import com.employee.management.request.dto.EmployeeUpdateRequestDto;
import com.employee.management.response.dto.EmployeeAuthResponseDto;
import com.employee.management.response.dto.EmployeeDetailsResponseDto;

public interface EmployeeService {

	public EmployeeAuthResponseDto authenticatingEmployeeDetails(EmployeeAuthRequestDto employeeRequestDto)
			throws CustomException;

	public EmployeeDetailsResponseDto createEmployee(EmployeeRequestDto employeeCreateRequestDto)
			throws CustomException;

	public List<EmployeeDetailsResponseDto> fetchAllEmployees() throws CustomException;

	public EmployeeDetailsResponseDto fetchEmployeeByEmployeeId(Integer employeeId) throws CustomException;

	public void deleteEmployeeByEmployeeId(Integer employeeId) throws CustomException;

	public EmployeeDetailsResponseDto updateEmployeeName(@Valid EmployeeUpdateRequestDto employeeCreateRequestDto,
			Integer employeeId) throws CustomException;

}
