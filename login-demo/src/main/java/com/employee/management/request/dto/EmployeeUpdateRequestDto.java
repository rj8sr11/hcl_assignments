package com.employee.management.request.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class EmployeeUpdateRequestDto implements Serializable {

	private static final long serialVersionUID = -7604186624445847129L;

	@NotNull(message = "Employee Name Can't be null")
	@Pattern(regexp = "^[a-zA-Z]*$", message = "EmployeeName must contain only Alphabets")
	@Size(min = 5, max = 100, message = "Employee name must be greater than 5 characters")
	private String employeeName;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

}
