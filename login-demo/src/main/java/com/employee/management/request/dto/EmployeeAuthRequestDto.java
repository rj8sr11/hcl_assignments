package com.employee.management.request.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EmployeeAuthRequestDto implements Serializable {

	private static final long serialVersionUID = -3339644611640406227L;

	@NotNull(message = "UserName Can't be null")
	@Size(min = 5, max = 100, message = "Username must be greater than 5 characters")
	private String userName;

	@NotNull(message = "Password Can't be null")
	@Size(min = 4, max = 100, message = "Password must be greater than 4 characters")
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
