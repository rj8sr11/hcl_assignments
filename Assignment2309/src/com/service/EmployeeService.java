package com.service;

import com.model.Employee;

/**
 * @author rajat.sharma
 *
 */
public class EmployeeService {

	public Employee comparingEmployeesSalary(Employee employee1, Employee employee2) {
		return employee1.getEmployeeSalary() > employee2.getEmployeeSalary() ? employee1 : employee2;
	}

	public float calculatingEmployeesSalaryTotal(Employee[] employees) {
		float sum = 0;
		for (int i = 0; i < employees.length; i++) {
			sum += employees[i].getEmployeeSalary();
		}
		return sum;
	}

	public Employee searchingEmployeeByEmployeeId(Employee[] employees, int employeeId) {
		for (int i = 0; i < employees.length; i++) {
			if (employees[i].getEmployeeId() == employeeId) {
				return employees[i];
			}
		}
		return null;

	}

	public Employee[] searchEmployeeDetailByEmployeeName(Employee[] employees, String employeeName) {
		boolean containNull = true;
		Employee[] empl = new Employee[employees.length];
		for (int i = 0; i < employees.length; i++) {
			if (employees != null && employees[i].getEmployeeName().equals(employeeName)) {
				containNull = false;
				empl[i] = employees[i];
			
			}
		}
		if(containNull) {
			return null;
		}
		return empl;
	}
}
