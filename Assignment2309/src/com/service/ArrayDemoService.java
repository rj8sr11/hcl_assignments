package com.service;

import java.util.Scanner;

/**
 * @author rajat.sharma
 *
 */
public class ArrayDemoService {
	public int sumOfArrayElements(Scanner sc) {
		int sum = 0 ;

		// the SOP's will be in main only logic will be there in service file
		// we can pass scanner from main method layer

		int size = sc.nextInt();

		int[] arrayElements = new int[size];

		System.out.println("Enter elements for array");
	
		for (int i = 0; i < arrayElements.length; i++) {
			arrayElements[i] = sc.nextInt();
			sum = sum + arrayElements[i];
		}
		
		
		sc.close();
		return sum;
	}
}
