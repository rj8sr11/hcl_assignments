package com.main;

import java.util.Scanner;

import com.service.ArrayDemoService;

/**
 * @author rajat.sharma
 *
 */
public class ArrayDemoMain {

	public static void main(String[] args) {
		ArrayDemoService arrayDemoService = new ArrayDemoService();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter size of array");
		int sumOfArrayElements = arrayDemoService.sumOfArrayElements(sc);
		System.out.println("Sum Of Array Elements Is:" + sumOfArrayElements);
		arrayDemoService = null;
	}

}
