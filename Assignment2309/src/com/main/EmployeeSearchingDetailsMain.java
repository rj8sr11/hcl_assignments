package com.main;

import com.model.Employee;
import com.service.EmployeeService;

/**
 * @author rajat.sharma
 *
 */
public class EmployeeSearchingDetailsMain {
	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Ten", 10100.10f);
		Employee employee2 = new Employee(20, "Ten", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);
		
		Employee employee6 = new Employee(10, "Ten", 1010.10f);
		Employee employee7 = new Employee(20, "Ten", 20200.20f);
		Employee employee8 = new Employee(30, "Twenty", 3030.30f);
		

		EmployeeService employeeService = new EmployeeService();

		Employee[] employees = new Employee[6];
		employees[0] = employee1;
		employees[1] = employee3;
		employees[2] = employee2;
		employees[3] = employee6;
		employees[4] = employee7;
		employees[5] = employee8;

		String employeeName = "Twenty";
		Employee[] employeeDetail = employeeService.searchEmployeeDetailByEmployeeName(employees, employeeName);
		if (employeeDetail != null) {
			for (int i = 0; i < employeeDetail.length; i++) {
				if (employeeDetail[i] != null) {
					System.out.println(
							" Employee Exists with details:" + "\n EmployeeID: " + employeeDetail[i].getEmployeeId()
									+ "\n EmployeeName:" + employeeDetail[i].getEmployeeName() + "\n Employee Salary: "
									+ employeeDetail[i].getEmployeeSalary());
				}
			}
		} else {
			System.out.println("NOT PRESENT");
		}

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employees = null;
		employeeService = null;

	}

}
