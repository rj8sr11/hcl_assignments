package com.main;

import com.model.Employee;
import com.service.EmployeeService;

/**
 * @author rajat.sharma
 *
 */
public class EmployeeSalaryTotalMain {
	public static void main(String[] args) {

		Employee employee1 = new Employee(10, "Ten", 10100.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);
		Employee employee3 = new Employee(30, "Thirty", 3030.30f);

		EmployeeService employeeService = new EmployeeService();

		Employee[] employees = new Employee[3];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		float salaryTotal = employeeService.calculatingEmployeesSalaryTotal(employees);
		System.out.println(" Total Salary Employees Taking: " + salaryTotal);

		employee1 = null;
		employee2 = null;
		employee3 = null;
		employeeService = null;

	}

}
