package com.main;

import com.model.Employee;
import com.service.EmployeeService;

/**
 * @author rajat.sharma
 *
 */
public class EmployeeSalaryComparisionMain {
	public static void main(String[] args) {

		Employee employee1 = new Employee(10, "Ten", 10100.10f);
		Employee employee2 = new Employee(20, "Twenty", 2020.20f);

		EmployeeService employeeService = new EmployeeService();

		Employee employee = employeeService.comparingEmployeesSalary(employee1, employee2);//displayMaximumSalary

		System.out.println(" Employee Drawing Higher Salary details:" + "\n EmployeeID: " + employee.getEmployeeId()
				+ "\n EmployeeName:" + employee.getEmployeeName() + "\n Employee Salary: "
				+ employee.getEmployeeSalary());

		employee1 = null;
		employee2 = null;
		employeeService = null;

	}
}
