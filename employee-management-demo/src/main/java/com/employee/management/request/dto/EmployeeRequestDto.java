package com.employee.management.request.dto;

import java.io.Serializable;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class EmployeeRequestDto implements Serializable {

	private static final long serialVersionUID = 1351475855970049875L;

	@NotNull(message = "Employee Name Can't be null")
	@Pattern(regexp = "^[a-zA-Z]*$", message = "EmployeeName must contain only Alphabets")
	@Size(min = 5, max = 100, message = "Employee name must be greater than 5 characters")
	private String employeeName;

	@NotNull(message = "Employee Age Can't be null")
	@Digits(integer = 3, fraction = 0, message = "Age must be in Number ")
	@Min(value = 18, message = "age must be minimum 18 years")
	@Max(value = 56, message = "age must be maximum upto 56 years")
	private Integer employeeAge;

	@NotNull(message = "UserName Can't be null")
	@Size(min = 5, max = 100, message = "Username must be greater than 5 characters")
	private String userName;

	@NotNull(message = "Password Can't be null")
	@Size(min = 5, max = 100, message = "Password must be greater than 5 characters")
	private String password;

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Integer getEmployeeAge() {
		return employeeAge;
	}

	public void setEmployeeAge(Integer employeeAge) {
		this.employeeAge = employeeAge;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
