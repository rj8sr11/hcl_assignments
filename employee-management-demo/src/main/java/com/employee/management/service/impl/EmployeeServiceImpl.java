package com.employee.management.service.impl;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.management.customexception.CustomException;
import com.employee.management.entity.Employee;
import com.employee.management.repository.EmployeeRepository;
import com.employee.management.request.dto.EmployeeAuthRequestDto;
import com.employee.management.request.dto.EmployeeRequestDto;
import com.employee.management.request.dto.EmployeeUpdateRequestDto;
import com.employee.management.response.dto.EmployeeAuthResponseDto;
import com.employee.management.response.dto.EmployeeDetailsResponseDto;
import com.employee.management.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public EmployeeAuthResponseDto authenticatingEmployeeDetails(EmployeeAuthRequestDto employeeRequestDto)
			throws CustomException {
		Employee employeeDetail = employeeRepository.findByUserNameAndPassword(employeeRequestDto.getUserName(),
				employeeRequestDto.getPassword());
		if (employeeDetail != null) {
			EmployeeAuthResponseDto employeeAuthResponse = new EmployeeAuthResponseDto();
			employeeAuthResponse.setEmployeeName(employeeDetail.getEmployeeName());
			employeeAuthResponse.setEmployeeAge(employeeDetail.getEmployeeAge());
			return employeeAuthResponse;
		} else {
			throw new CustomException("INVALID CREDENTIALS");
		}

	}

	@Transactional
	@Override
	public EmployeeDetailsResponseDto createEmployee(EmployeeRequestDto employeeCreateRequestDto)
			throws CustomException {
		Employee employeeExists = employeeRepository.findByUserName(employeeCreateRequestDto.getUserName());
		if (employeeExists != null) {
			throw new CustomException("Employee with username already exists please try with another user name");
		} else {
			ModelMapper modelMapper = new ModelMapper();
			Employee employeeDetails = modelMapper.map(employeeCreateRequestDto, Employee.class);
			Employee employeeSaved = employeeRepository.save(employeeDetails);
			EmployeeDetailsResponseDto employeeCreateResponse = modelMapper.map(employeeSaved,
					EmployeeDetailsResponseDto.class);
			if (employeeCreateResponse != null) {
				return employeeCreateResponse;
			} else {
				throw new CustomException("Exception While Creating Employee");
			}
		}

	}

	@Override
	public List<EmployeeDetailsResponseDto> fetchAllEmployees() throws CustomException {
		List<Employee> employeesDetails = employeeRepository.findAll();
		if (employeesDetails.size() > 0 && employeesDetails != null) {
			Type listType = new TypeToken<List<EmployeeDetailsResponseDto>>() {
			}.getType();
			ModelMapper modelMapper = new ModelMapper();
			List<EmployeeDetailsResponseDto> postDtoList = modelMapper.map(employeesDetails, listType);
			return postDtoList;
		} else {
			throw new CustomException("No Record Exists");
		}
	}

	@Override
	public EmployeeDetailsResponseDto fetchEmployeeByEmployeeId(Integer employeeId) throws CustomException {
		Optional<Employee> employeeExist = employeeRepository.findById(employeeId);
		if (employeeExist.isPresent()) {
			ModelMapper modelMapper = new ModelMapper();
			EmployeeDetailsResponseDto employeeDetail = modelMapper.map(employeeExist.get(),
					EmployeeDetailsResponseDto.class);
			return employeeDetail;
		} else {
			throw new CustomException("No Record Exists with entered Employee Id");
		}

	}

	@Transactional
	@Override
	public void deleteEmployeeByEmployeeId(Integer employeeId) throws CustomException {
		employeeRepository.deleteById(employeeId);
	}

	@Transactional
	@Override
	public EmployeeDetailsResponseDto updateEmployeeName(@Valid EmployeeUpdateRequestDto employeeUpdateRequestDto,
			Integer employeeId) throws CustomException {
		Optional<Employee> employeeExist = employeeRepository.findById(employeeId);
		if (employeeExist.isPresent()) {
			Employee updateEmploye = employeeExist.get();
			updateEmploye.setEmployeeName(employeeUpdateRequestDto.getEmployeeName());
			Employee employeeSaved = employeeRepository.save(updateEmploye);
			ModelMapper modelMapper = new ModelMapper();
			EmployeeDetailsResponseDto employeeCreateResponse = modelMapper.map(employeeSaved,
					EmployeeDetailsResponseDto.class);
			if (employeeCreateResponse != null) {
				return employeeCreateResponse;
			} else {
				throw new CustomException("Exception While Updating Employee");
			}

		} else {
			throw new CustomException("No Employee Exists with entered Employee Id");
		}

	}

}
