package com.hcl.pp.validator;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserValidator implements Serializable {

	private static final long serialVersionUID = 6663190996567312288L;

	@NotNull(message = "userName can't be null")
	@NotEmpty(message = "userName can't be empty")
	private String userName;

	@NotNull(message = "password can't be null")
	@NotEmpty(message = "password can't be empty")
	private String password;

	@NotNull(message = "confirm password can't be null")
	@NotEmpty(message = "confirm password can't be empty")
	private String confirmPassword;

}
