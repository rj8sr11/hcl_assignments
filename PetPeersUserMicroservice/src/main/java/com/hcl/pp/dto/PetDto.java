package com.hcl.pp.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PetDto implements Serializable {

	private static final long serialVersionUID = 3929315775436982024L;

	private Long id;

	private String name;

	private Integer age;

	private String place;

	private Long userId;

}
